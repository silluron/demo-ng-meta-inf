#!/bin/bash

#docker login if needed here

mkdir tmp
cd tmp
git clone git@gitlab.com:silluron/angular-example-parent.git
cd angular-example-parent
cp ../../Dockerfile ./
cp ../../../../all/.dockerignore ./
docker build -t asilluron/demo-docker-development-angular:0.0.5 .
docker push asilluron/demo-docker-development-angular:0.0.5
cd ../../
rm -rf tmp 
