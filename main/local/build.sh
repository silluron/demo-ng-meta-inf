#!/bin/bash

#docker login if needed here

mkdir tmp
cd tmp
git clone git@gitlab.com:silluron/angular-example-parent.git
cd angular-example-parent
cp ../../Dockerfile ./
cp ../../../../all/.dockerignore ./
docker build -t asilluron/demo-docker-local-angular:0.0.1 .
docker push asilluron/demo-docker-local-angular:0.0.1
cd ../../
rm -rf tmp 
